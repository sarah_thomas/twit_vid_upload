FactoryGirl.define do
	factory :video do
		title { Faker::Lorem.sentence } 
		description { Faker::Lorem.paragraph}
		factory :invalid_video do
			title nil
		end
	end
end