require 'rails_helper'

describe VideosController do
	
	describe 'GET#show' do
		
		it "assigns the requested video to @video" do
			video = create(:video)
			get :show, id: video
			expect(assigns(:video)).to eq video
  		end

  		it "renders the :show template" do
			video = create(:video)
			get :show, id: video
			expect(response).to render_template :show
		end
	end

	describe 'GET#new' do
	
		it "assigns a new video to @video" do
			get :new
			expect(assigns(:video)).to be_a_new(Video)
		end

		it "renders the :new template" do
			get :new
			expect(response).to render_template :new
		end
	end

	describe 'GET#edit' do
		
		it "assigns the requested video to @video" do
			video = create(:video)
			get :edit, id: video
			expect(assigns(:video)).to eq video
		end

		it "renders the :edit template" do
			video = create(:video)
			get :edit, id: video
			expect(response).to render_template :edit
		end

	end

	describe 'GET#index' do
		before(:each) {Video.destroy_all}
		
		it "lists the entries in the phonebook" do
			video = create(:video)
			get :index
			expect(assigns(:videos)).to match_array [video]
		end

			it "renders the :index template" do
			video = create(:video)
			get :index
			expect(response).to render_template :index
		end

	end

	describe 'POST#create' do
		context "with valid attributes" do
			it "saves the new video in the database" do
				expect {
					post :create, video: attributes_for(:video)
				}.to change(Video, :count).by(1)

			end

			it "redirects to videos#show" do
				post :create, video: attributes_for(:video)
				expect(response).to redirect_to video_path(assigns[:video])
			end
		end

		context "with invalid attributes" do
			it "does not save the new video in the database" do
          		expect{
			 		post :create, video: attributes_for(:invalid_video)
 				}.not_to change(Video, :count)
			end

			it "re-renders the new template" do
				post :create, video: attributes_for(:invalid_video)
				expect(response).to render_template :new
			end
		end
	end

	describe 'PATCH#update' do
		before :each do
			@video = create(:video,
				title: 'Tina',
				description: 'dewquygdguq')
		end
		context "with valid attributes" do
			it "locates the requested @video" do
				patch :update, id: @video, video: attributes_for(:video)
				expect(assigns(:video)).to eq(@video)
			end
			it "changes the @video's attributes" do
				patch :update, id: @video, video: attributes_for(:video, title: 'Tinsy')
				@video.reload
				expect(@video.title).to eq('Tinsy')
			end

			it "redirects to the updated video" do
				patch :update, id: @video, video:  attributes_for(:video)
				expect(response).to redirect_to @video
			end
		end

		context "with invalid attributes" do
			it "does not change the video's attributes" do
				patch :update, id: @video, video: attributes_for(:video, title: 'Tinsy', 
					description: nil)
				@video.reload
				expect(@video.title).not_to eq('Tinsy')
			end
			it "re-renders the edit template" do
				patch :update, id: @video,
				video: attributes_for(:invalid_video)
				expect(response).to render_template :edit
			end
		end
	end

	describe 'DELETE #destroy' do
		before :each do
			@video = create(:video)
		end

 		it "deletes the video" do
 			expect{
 				delete :destroy, id: @video
 			}.to change(Video,:count).by(-1)
 		end

 		it "redirects to videos#index" do
 			delete :destroy, id: @video
 			expect(response).to redirect_to videos_url
 		end
 	end

end