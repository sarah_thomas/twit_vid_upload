require 'rails_helper'

describe Video do 

	it "has a valid factory" do
		expect(build(:video)).to be_valid
	end

	it "is valid with title and description" do
		video = build(:video)
		expect(video).to be_valid
	end
	
	it "is invalid without title" do
		video = build(:video, title: nil)
		video.valid?
		expect(video.errors[:title]).to include("can't be blank")
	end
	
	it "is invalid without description" do
		video = build(:video, description: nil)
		video.valid?
		expect(video.errors[:description]).to include("can't be blank")
	end

	it "is invalid with a duplicate title" do
		FactoryGirl.create(:video, title: 'hello')
		video = build(:video, title: 'hello')
		video.valid?
		expect(video.errors[:title]).to include("has already been taken")
	end

end