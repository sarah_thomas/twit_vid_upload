json.array!(@videos) do |video|
  json.extract! video, :id, :title, :description, :file, :remote_file_url
  json.url video_url(video, format: :json)
end
