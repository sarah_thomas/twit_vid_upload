module API
	module V1
		class Videos < Grape::API
			include API::V1::Defaults

			resource :videos do
				desc "Return all videos"
				get "", root: :videos do
					Video.all
				end
				
				desc "Return a video"
				params do
					requires :id, type: String, desc: "ID of the video"
				end

				get ":id", root: "video" do
					Video.where(id:permitted_params[:id]).first!
				end
			end
		end
	end
end
