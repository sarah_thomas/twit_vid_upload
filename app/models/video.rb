class Video < ActiveRecord::Base
	validates :title, presence: true, uniqueness: true
	validates :description, presence: true
	belongs_to :user
	mount_uploader :file, FileUploader
end
